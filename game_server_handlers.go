package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func restRegisterGameServer(w http.ResponseWriter, r *http.Request) {
	var server = Server{}
	_ = json.NewDecoder(r.Body).Decode(&server)

	for _, player := range players { // Todo: pull from matchmaker
		ticket := tickets[player.UID]
		ticket.Host = server.IPV4
		ticket.Port = server.Port
		ticket.Ready = true
		tickets[player.UID] = ticket
	}

	log.Printf("Register Server %v\n", server.SessionID)
	registrationResponse := ServerRegistrationResponse{
		players,
		5,
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(registrationResponse)

	players = players[:0] // Todo: remove
}

func restEndMatchGameServer(w http.ResponseWriter, r *http.Request) {
	var endgame = EndGame{}
	_ = json.NewDecoder(r.Body).Decode(&endgame)
	var playerUIDs []uint64
	for _, player := range endgame.Players {
		playerUIDs = append(playerUIDs, player.UID)
	}
	players, success := bulkGetPlayer(playerUIDs)
	if !success {
		http.Error(w, "Failed to find player(s)", http.StatusInternalServerError)
		return
	}
	var dbPlayers map[uint64]PlayerDB
	for _, player := range players {
		dbPlayers[player.UID] = player
	}
	var updatedPlayers []PlayerDB
	for _, update := range endgame.Players {
		player := dbPlayers[update.UID]
		if player.TopScore < update.Score {
			player.TopScore = update.Score
			updatedPlayers = append(updatedPlayers, player)
		}
	}
	success = bulkUpdatePlayer(updatedPlayers)
	if !success {
		http.Error(w, "Failed to update player(s)", http.StatusInternalServerError)
		return
	}
}
