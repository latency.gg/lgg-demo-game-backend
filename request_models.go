package main

type Server struct {
	IPV4      string `json:"ipv4"`
	Port      int    `json:"port"`
	SessionID string `json:"session_id"`
}

type NewTicketRequest struct {
	Mode string `json:"mode"`
}

type PlayerUpdate struct {
	UID         uint64 `json:"uid"`
	DisplayName string `json:"display_name"`
}

type EndGamePlayer struct {
	UID   uint64 `json:"uid"`
	Score uint32 `json:"score"`
}

type EndGame struct {
	SessionID string          `json:"session_id"`
	Players   []EndGamePlayer `json:"players"`
}
