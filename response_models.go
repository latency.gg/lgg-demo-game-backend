package main

type PlayerTicketResponse struct {
	ID        string `json:"id"`
	Ready     bool   `json:"ready"`
	Host      string `json:"host"`
	Port      int    `json:"port"`
	PlayerUID uint64 `json:"uid"`
}

type PlayerDisplay struct {
	UID         uint64 `json:"uid"`
	DisplayName string `json:"display_name"`
	TopScore    uint32 `json:"top_score"`
}

type PlayerRegistrationResponse struct {
	UID         uint64 `json:"uid"`
	DisplayName string `json:"display_name"`
	Password    string `json:"password"`
}

type ServerRegistrationResponse struct {
	Players    []PlayerDisplay `json:"players"`
	MaxLatency uint64          `json:"max_latency"`
}
