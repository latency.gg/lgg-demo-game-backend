package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4/pgxpool"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"open-match.dev/open-match/pkg/pb"
	"os"
)

var players []PlayerDisplay
var tickets = make(map[uint64]PlayerTicketResponse)
var uidPlayer = make(map[string]PlayerDB)
var db *pgxpool.Pool
var Omconn *grpc.ClientConn
var OmFe pb.FrontendServiceClient

func newREST() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/player", restPlayerRegister).Methods("POST")
	r.HandleFunc("/player/{UID}", restPlayerGet).Methods("GET")
	r.HandleFunc("/player/{UID}", restPlayerUpdate).Methods("PUT")
	r.HandleFunc("/latency", restBeaconsGet).Methods("GET")
	r.HandleFunc("/server", restRegisterGameServer).Methods("POST")
	r.HandleFunc("/server", restEndMatchGameServer).Methods("PUT")
	r.HandleFunc("/ticket", restOpenTicket).Methods("POST")
	r.HandleFunc("/ticket", restPollTicket).Methods("GET")
	r.HandleFunc("/ticket", restDeleteTicket).Methods("DELETE")
	amw := authenticationMiddleware{make(map[string]bool), make(map[string]bool)}
	amw.bypass["/player"] = true
	amw.bearerPaths["/server"] = true
	r.Use(amw.Middleware)
	return r
}

func main() {
	router := newREST()
	dbpool, err := pgxpool.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	db = dbpool
	defer dbpool.Close()
	Omconn, err = grpc.Dial(omFrontendEndpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to connect to Open Match, got %v", err)
	}

	defer Omconn.Close()
	OmFe = pb.NewFrontendServiceClient(Omconn)
	log.Fatal(http.ListenAndServe(":5000", router))
}
