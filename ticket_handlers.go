package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"open-match.dev/open-match/pkg/pb"
)

func restOpenTicket(w http.ResponseWriter, r *http.Request) {
	u := r.Header.Get("X-UID")
	player, found := getPlayer(u)
	if !found {
		http.Error(w, "Failed to find player", http.StatusNotFound)
		return
	}
	var ticketRequest = NewTicketRequest{}
	_ = json.NewDecoder(r.Body).Decode(&ticketRequest)
	log.Printf("Player %v\n", player.UID)

	ticket := &pb.Ticket{
		SearchFields: &pb.SearchFields{
			DoubleArgs: map[string]float64{
				"skill": float64(player.TopScore),
				// top5 latencies
			},
			StringArgs: map[string]string{
				// top5 locations
			},
			Tags: []string{
				ticketRequest.Mode,
			},
		},
	}
	req := &pb.CreateTicketRequest{
		Ticket: ticket,
	}

	resp, err := OmFe.CreateTicket(context.Background(), req)
	if err != nil {
		log.Printf("Failed to create Ticket, got %s", err.Error())
		http.Error(w, "Failed to create Ticket", http.StatusInternalServerError)
		return
	}

	player.CurrentTicket = resp.GetId()
	player, success := updatePlayer(player)
	if !success {
		log.Printf("Failed to update DB, got %s", err.Error())
		http.Error(w, "Failed to create Ticket", http.StatusInternalServerError)
		return
	}
	responseTicket, err := AssignmentToTicketResponse(resp.GetId(), player, resp.GetAssignment())

	if err != nil {
		log.Printf("Failed to process Assignment, got %s", err.Error())
		http.Error(w, "Failed to create ticket", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(responseTicket)
}

func restPollTicket(w http.ResponseWriter, r *http.Request) {
	u := r.Header.Get("X-UID")
	player, found := getPlayer(u)
	if !found {
		http.Error(w, "Failed to find player", http.StatusNotFound)
		return
	}
	resp, err := OmFe.GetTicket(context.Background(), &pb.GetTicketRequest{TicketId: player.CurrentTicket})
	if err != nil {
		log.Fatalf("Failed to Get Ticket %v, got %s", player.CurrentTicket, err.Error())
	}

	responseTicket, err := AssignmentToTicketResponse(resp.GetId(), player, resp.GetAssignment())

	if err != nil {
		log.Printf("Failed to process Assignment, got %s", err.Error())
		http.Error(w, "Failed to get ticket", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(responseTicket)
}

func restDeleteTicket(w http.ResponseWriter, r *http.Request) {
	u := r.Header.Get("X-UID")
	player, found := getPlayer(u)

	if !found {
		http.Error(w, "Failed to find player", http.StatusNotFound)
		return
	}

	_, err := OmFe.DeleteTicket(context.Background(), &pb.DeleteTicketRequest{TicketId: player.CurrentTicket})

	if err != nil {
		log.Printf("Failed to process Assignment, got %s", err.Error())
		http.Error(w, "Failed to delete ticket", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(201)
	return
}
