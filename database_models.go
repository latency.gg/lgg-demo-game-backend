package main

type PlayerDB struct {
	UID           uint64
	DisplayName   string
	TopScore      uint32
	PasswordHash  string
	CurrentTicket string
}
