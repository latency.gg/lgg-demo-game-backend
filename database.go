package main

import (
	"context"
	"github.com/jackc/pgx/v4"
	"log"
	"strconv"
)

func createPlayer(player PlayerDB) (PlayerDB, bool) {
	var found = false
	tx, err := db.BeginTx(context.Background(), pgx.TxOptions{pgx.Serializable, pgx.ReadWrite, pgx.NotDeferrable})
	if err == nil {
		tag, err := tx.Exec(context.Background(), "INSERT INTO players (display_name, top_score, pw_pash) VALUES ($1, $2, $3)", player.DisplayName, player.TopScore, player.PasswordHash)
		found = tag.RowsAffected() == 1 && err == nil
	}
	if !found {
		err = tx.Rollback(context.Background())
	} else {
		err = tx.Commit(context.Background())
	}
	if err != nil {
		log.Printf("Commit or Rollback returned error: %v", err)
	}
	return player, found && err == nil
}

func getPlayer(sUID string) (PlayerDB, bool) {
	var player = PlayerDB{}
	var found = false
	uid, err := strconv.ParseUint(sUID, 10, 64)
	if err == nil {
		err = db.QueryRow(context.Background(), "SELECT uid, display_name, top_score, pw_hash, current_ticket FROM players WHERE uid=$1", uid).Scan(&(player.UID), &(player.DisplayName), &(player.TopScore), &(player.PasswordHash), &(player.CurrentTicket))
		found = err == nil
	}
	return player, found
}

func getPlayerByName(name string) (PlayerDB, bool) {
	var player = PlayerDB{}
	var found = false
	err := db.QueryRow(context.Background(), "SELECT uid, display_name, top_score, pw_hash, current_ticket FROM players WHERE display_name=$1", name).Scan(&(player.UID), &(player.DisplayName), &(player.TopScore), &(player.PasswordHash), &(player.CurrentTicket))
	found = err == nil
	return player, found
}

func updatePlayer(player PlayerDB) (PlayerDB, bool) {
	var found = false
	tx, err := db.BeginTx(context.Background(), pgx.TxOptions{pgx.Serializable, pgx.ReadWrite, pgx.NotDeferrable})
	if err == nil {
		tag, err := db.Exec(context.Background(), "UPDATE players SET display_name=$2, top_score=$3, current_ticket=$4 WHERE uid=$1", player.UID, player.DisplayName, player.TopScore, player.CurrentTicket)
		found = tag.RowsAffected() == 1 && err == nil
	}
	if !found {
		err = tx.Rollback(context.Background())
	} else {
		err = tx.Commit(context.Background())
	}
	if err != nil {
		log.Printf("Commit or Rollback returned error: %v", err)
	}
	return player, found && err == nil
}

func bulkGetPlayer(playerUIDs []uint64) ([]PlayerDB, bool) {
	var success = true
	var players []PlayerDB
	tx, err := db.BeginTx(context.Background(), pgx.TxOptions{pgx.ReadCommitted, pgx.ReadOnly, pgx.NotDeferrable})
	if err == nil {
		rows, err := tx.Query(context.Background(), "SELECT uid, display_name, top_score, pw_hash, current_ticket FROM players WHERE uid=$1", playerUIDs)
		if err == nil {
			var player = PlayerDB{}
			for rows.Next() {
				err := rows.Scan(&(player.UID), &(player.DisplayName), &(player.TopScore), &(player.PasswordHash), &(player.CurrentTicket))
				if err != nil {
					success = false
					break
				}
			}
			players = append(players, player)
		}
	}
	return players, success
}

func bulkUpdatePlayer(players []PlayerDB) bool {
	var success = false
	tx, err := db.BeginTx(context.Background(), pgx.TxOptions{pgx.Serializable, pgx.ReadWrite, pgx.NotDeferrable})
	if err == nil {
		for _, player := range players {
			tag, err := db.Exec(context.Background(), "UPDATE players SET display_name=$2, top_score=$3, current_ticket=$4 WHERE uid=$1", player.UID, player.DisplayName, player.TopScore, player.CurrentTicket)
			success = tag.RowsAffected() == 1 && err == nil
			if !success {
				break
			}
		}
	}
	if !success {
		err = tx.Rollback(context.Background())
	} else {
		err = tx.Commit(context.Background())
	}
	if err != nil {
		log.Printf("Commit or Rollback returned error: %v", err)
		return false
	}
	return success
}
