package main

import (
	"math/rand"
	"open-match.dev/open-match/pkg/pb"
	"strconv"
	"strings"
)

func RandomString(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

func AssignmentToTicketResponse(id string, player PlayerDB, assignment *pb.Assignment) (PlayerTicketResponse, error) {
	ready := false
	host := ""
	var port int64 = 0
	var err error
	if assignment != nil {
		ready = true
		connection := assignment.GetConnection()
		parts := strings.Split(connection, ":")
		host = parts[0]
		port, err = strconv.ParseInt(parts[1], 10, 32)
	}
	responseTicket := PlayerTicketResponse{
		id,
		ready,
		host,
		int(port),
		player.UID,
	}
	return responseTicket, err

}
