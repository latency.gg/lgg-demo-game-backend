package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/raja/argon2pw"
	"log"
	"net/http"
)

func restPlayerRegister(w http.ResponseWriter, r *http.Request) {
	password := RandomString(32)
	var playerPatch = PlayerUpdate{}
	_ = json.NewDecoder(r.Body).Decode(&playerPatch)
	hashedPassword, err := argon2pw.GenerateSaltedHash(password)
	if err != nil {
		log.Printf("Hash generated returned error: %v", err)
		http.Error(w, "Failed to create player", http.StatusInternalServerError)
		return
	}
	_, conflict := getPlayerByName(playerPatch.DisplayName)
	if conflict {
		http.Error(w, "Failed to create player", http.StatusConflict)
		return
	}
	player := PlayerDB{
		0,
		playerPatch.DisplayName,
		0,
		hashedPassword,
		"",
	}
	player, success := createPlayer(player)
	if !success {
		http.Error(w, "Failed to create player", http.StatusInternalServerError)
		return
	}
	jdata := PlayerRegistrationResponse{
		player.UID,
		playerPatch.DisplayName,
		password,
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(jdata)
}

func restPlayerGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	UID := vars["UID"]
	if player, found := uidPlayer[UID]; found {
		playerResponse := PlayerDisplay{
			player.UID,
			player.DisplayName,
			player.TopScore,
		}
		log.Printf("Player %v\n", playerResponse.UID)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(player)
	} else {
		http.Error(w, "Failed to find player", http.StatusNotFound)
	}
}

func restPlayerUpdate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	UID := vars["UID"]
	u := r.Header.Get("X-UID")
	if UID != u {
		http.Error(w, "Forbidden", http.StatusForbidden)
		return
	}
	var playerPatch = PlayerUpdate{}
	_ = json.NewDecoder(r.Body).Decode(&playerPatch)
	player, found := getPlayer(UID)
	if found && (player.DisplayName == playerPatch.DisplayName) {
		log.Printf("Player %v\n", player.UID)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		playerResponse := PlayerDisplay{
			player.UID,
			player.DisplayName,
			player.TopScore,
		}
		json.NewEncoder(w).Encode(playerResponse)
		return
	} else if !found {
		http.Error(w, "Failed to find player", http.StatusNotFound)
		return
	}
	_, conflict := getPlayerByName(playerPatch.DisplayName)
	if conflict {
		http.Error(w, "Failed to update player", http.StatusConflict)
		return
	}
	player.DisplayName = playerPatch.DisplayName
	player, success := updatePlayer(player)
	if success {
		playerResponse := PlayerDisplay{
			player.UID,
			player.DisplayName,
			player.TopScore,
		}
		player.DisplayName = playerPatch.DisplayName
		log.Printf("Player %v\n", playerResponse.UID)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(playerResponse)
		return
	}
	http.Error(w, "Failed to update player", http.StatusInternalServerError)
	return
}
