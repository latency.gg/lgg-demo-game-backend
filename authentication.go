package main

import (
	"github.com/raja/argon2pw"
	"log"
	"net/http"
	"strings"
)

type authenticationMiddleware struct {
	bypass      map[string]bool
	bearerPaths map[string]bool
}

func (amw *authenticationMiddleware) BearerAuth(w http.ResponseWriter, r *http.Request) bool {
	if auth := r.Header.Get("Authorization"); auth != "" {
		if _, found := amw.bearerPaths[r.URL.Path]; found && strings.HasPrefix(auth, "Bearer ") {
			token := auth[7:]
			log.Printf("token: %s", token)
			return true
		}
	}
	return false
}

func (amw *authenticationMiddleware) BasicAuth(w http.ResponseWriter, r *http.Request) bool {
	u, p, ok := r.BasicAuth()
	if ok {
		if player, found := getPlayer(u); found {
			valid, err := argon2pw.CompareHashWithPassword(player.PasswordHash, p)
			if err == nil || valid {
				r.Header.Add("X-UID", u)
				return true
			}
		}
	}
	log.Printf("Player could not be found or failed validation %s", u)
	return false
}

func (amw *authenticationMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.Header.Del("X-UID")
		log.Printf("path: %s", r.URL.Path)
		auth := r.Header.Get("Authorization")
		_, found := amw.bypass[r.URL.Path]
		if (auth != "" && (amw.BearerAuth(w, r) || amw.BasicAuth(w, r))) || found {
			next.ServeHTTP(w, r)
		} else {
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
}
